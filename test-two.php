<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <title>Vintex test</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<div class="container-fluid">
    <div class="row m-3">
        <div class="col-lg-3 d-none d-lg-block"></div>
        <div class="col-lg-6 col-12">
            <div class="row">
                <div class="col-12 col-lg-9">
                    <label for="address"><strong>Адрес объекта</strong></label>
                    <input type="text" class="form-control" id="address" name="location" required="required">
                </div>
                <div class="col-12 col-lg-3 mt-3 mt-lg-0">
                    <div class="d-none d-lg-block"><label for="address">&nbsp;</label></div>
                    <button id="search" type="button" class="btn btn-primary form-control">Найти на карте</button>
                </div>
            </div>
        </div>
        <div class="col-lg-3 d-none d-lg-block"></div>
    </div>
    <div class="row">
        <div class="col-lg-3 d-none d-lg-block"></div>
        <div class="col-lg-6 col-12">
            <div id="map" style="height: 400px"></div>
        </div>
        <div class="col-lg-3 d-none d-lg-block"></div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="https://api-maps.yandex.ru/2.1/?lang=ru_RU&amp;scroll=true"></script>
<script type="text/javascript" src="js/yMap.js"></script>
<script type="text/javascript">
    ymaps.ready(init);
</script>
</body>
</html>