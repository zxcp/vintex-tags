/** Created by Anton on 22.09.2018. */

var yMap, mark;
function init() {
    yMap = new ymaps.Map("map", {
        center: [56.852775, 53.211463],
        zoom: 7,
        controls: ['geolocationControl', 'typeSelector', 'fullscreenControl', 'zoomControl', ]
    });

    mark = new ymaps.Placemark([56.852775, 53.211463], {},  {
        iconLayout: 'default#image',
        iconImageHref: 'img/mark.png',
        iconImageSize: [30, 42],
        iconImageOffset: [-16, -45],
        draggable: true
    });

    mark.events.add('dragend', function () {
        ymaps.geocode(mark.geometry.getCoordinates()).then(function (res) {
            var addr = res.geoObjects.get(0).properties.get('description') + ', ' + res.geoObjects.get(0).properties.get('name');
            $('#address').get(0).value = addr.substr(addr.indexOf(',') + 2);
        });
    });

    var btn = new ymaps.control.Button({//'<img src="img/btn-mark.png">'
        data: {
            image: 'img/btn-mark.png',
            title: 'Поставить метку'
        },
        options: {
            selectOnClick: false
        }
    });
    yMap.controls.add(btn, {
        // float: "left"
    });
    yMap.geoObjects.add(mark);
}

$('#address').on('keydown', function (e) {
    if(e.which === 13) $('#search').click();
});

$('#search').on('click', function () {
    ymaps.geocode($('#address').get(0).value).then(function (res) {
        var coords = res.geoObjects.get(0).geometry.getCoordinates();
        mark.geometry.setCoordinates(coords);
        var addr = res.geoObjects.get(0).properties.get('description') + ', ' + res.geoObjects.get(0).properties.get('name');
        $('#address').get(0).value = addr.substr(addr.indexOf(',') + 2);
        yMap.setCenter(coords, 7);
    });
});